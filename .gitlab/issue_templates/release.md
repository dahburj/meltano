## Release To Do Items

### Publish

- [ ] Record [Release Process](https://www.meltano.com/docs/contributing.html#release-process) to kick off pipelines
- [ ] Verify users can successfully install new version on local machine
- [ ] Deploy to DigitalOcean

### Marketing

- [ ] Write blog post about release
- [ ] Add release video to blog post
  - [ ] Tweet out release blog post
  - [ ] Publish blog post to on Meltano Slack channel
- [ ] Record [Speedrun Workflow](https://www.meltano.com/docs/contributing.html#speedruns)
  - [ ] Tweet out speedrun
  - [ ] Publish speedrun to on Meltano Slack channel
  - [ ] Update home page in docs to have the latest speedrun embed
